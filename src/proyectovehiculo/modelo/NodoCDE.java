/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.modelo;

/**
 *
 * @author Usuario
 */
public class NodoCDE {
    
    private Ficha dato;
    private NodoCDE siguiente;
    private NodoCDE anterior;

    public NodoCDE(Ficha dato) {
        this.dato = dato;
    }
    
    

    public Ficha getDato() {
        return dato;
    }

    public void setDato(Ficha dato) {
        this.dato = dato;
    }

    public NodoCDE getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoCDE siguiente) {
        this.siguiente = siguiente;
    }

    public NodoCDE getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoCDE anterior) {
        this.anterior = anterior;
    }
    
    
    
    
}
