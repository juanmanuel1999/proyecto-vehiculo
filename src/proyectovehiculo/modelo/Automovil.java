/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.modelo;

import java.util.Date;

/**
 *
 * @author JUANMA
 */
public class Automovil extends Vehiculo{ 
    
    private boolean pasaCintas;
    
    public Automovil(boolean pasaCintas, String placa, Date fechaHoraEntra, String ciudad) {
        super(placa, fechaHoraEntra, ciudad);
        this.pasaCintas = pasaCintas;
    }

    public boolean isPasaCintas() {
        return pasaCintas;
    }

    public void setPasaCintas(boolean pasaCintas) {
        this.pasaCintas = pasaCintas;
    }
    
    
    
}
