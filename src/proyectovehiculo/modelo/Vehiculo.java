/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.modelo;

import java.util.Date;

/**
 *
 * @author @fecha 18/09/2017
 * @hora 03:28:24 PM
 */
public class Vehiculo {

    private String placa;
    private Date fechaHoraEntra;
    private Date fechaHoraSalida;
    private double valorAPagar;
    private String ciudad;

    public Vehiculo() {
    }

    public Vehiculo(String placa, Date fechaHoraEntra, String ciudad) {
        this.placa = placa;
        this.fechaHoraEntra = fechaHoraEntra;
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Date getFechaHoraEntra() {
        return fechaHoraEntra;
    }

    public void setFechaHoraEntra(Date fechaHoraEntra) {
        this.fechaHoraEntra = fechaHoraEntra;
    }

    public Date getFechaHoraSalida() {
        return fechaHoraSalida;
    }

    public void setFechaHoraSalida(Date fechaHoraSalida) {
        this.fechaHoraSalida = fechaHoraSalida;
    }

    public double getValorAPagar() {
        return valorAPagar;
    }

    public void setValorAPagar(double valorAPagar) {
        this.valorAPagar = valorAPagar;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "placa=" + placa + ", fechaHoraEntra=" + fechaHoraEntra + ", fechaHoraSalida=" + fechaHoraSalida + ", valorAPagar=" + valorAPagar + '}';
    }
}
