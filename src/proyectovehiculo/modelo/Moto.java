/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.modelo;

import java.util.Date;

/**
 *
 * @author JUANMA
 */
public class Moto extends Vehiculo {

    private boolean casco;

    public Moto(boolean casco, String placa, Date fechaHoraEntra, String ciudad) {
        super(placa, fechaHoraEntra, ciudad);
        this.casco = casco;
    }

    public boolean isCasco() {
        return casco;
    }

    public void setCasco(boolean casco) {
        this.casco = casco;
    }

}
