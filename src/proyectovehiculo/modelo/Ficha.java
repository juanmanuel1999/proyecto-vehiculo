/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.modelo;

/**
 *
 * @author Usuario
 */
public class Ficha {
    
   private String color;
   private int numero;

    public Ficha() {
    }

    public Ficha(String color, int numero) {
        this.color = color;
        this.numero = numero;
    }

   
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Ficha{" + "color=" + color + ", numero=" + numero + '}';
    }

  

   
    
    
    
}
