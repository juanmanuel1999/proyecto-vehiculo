/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.modelo;

import java.util.Date;

/**
 *
 * @author JUANMA
 */
public class Buseta extends Vehiculo{
    
    private byte numeroAsientos;

    public Buseta(byte numeroAsientos, String placa, Date fechaHoraEntra, String ciudad) {
        super(placa, fechaHoraEntra, ciudad);
        this.numeroAsientos = numeroAsientos;
    }

    

    public byte getNumeroAsientos() {
        return numeroAsientos;
    }

    public void setNumeroAsientos(byte numeroAsientos) {
        this.numeroAsientos = numeroAsientos;
    }
    
    
}
