/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.controlador.excepciones;

/**
 *
 * @author Usuario
 */
public class ParqueaderoExcepcion extends Exception{

    public ParqueaderoExcepcion() {
    }

    public ParqueaderoExcepcion(String message) {
        super(message);
    }

    public ParqueaderoExcepcion(String message, Throwable cause) {
        super(message, cause);
    }

    public ParqueaderoExcepcion(Throwable cause) {
        super(cause);
    }
    
}
