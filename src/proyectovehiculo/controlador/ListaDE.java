/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.controlador;

import proyectovehiculo.modelo.Nodo;
import proyectovehiculo.modelo.NodoDE;
import proyectovehiculo.modelo.Vehiculo;

/**
 *
 * @author Usuario
 */
public class ListaDE {

    private NodoDE cabeza;

    public ListaDE() {
    }

    public NodoDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoDE cabeza) {
        this.cabeza = cabeza;
    }

    public void adicionarNodoEnElFinalDE(Vehiculo dato) {
        if (cabeza != null)//preguntar si cabeza tiene alguien pegado
        {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            temp.setSiguiente(new NodoDE(dato));
            temp.getSiguiente().setAnterior(temp);
        } else {
            cabeza = new NodoDE(dato);
        }
    }
    
    
    public NodoDE irAlUltimoDE() {
        NodoDE resultado = cabeza;
        if (cabeza != null) {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                resultado = temp.getSiguiente();
                temp = temp.getSiguiente();
            }
            return resultado;
        } else {
            return resultado;
        }
    }


    public void adicionarNodoEnElInicio(Vehiculo dato) {
        if (cabeza != null) {
            NodoDE nuevo = new NodoDE(dato);
            nuevo.setSiguiente(cabeza);
            cabeza.setAnterior(nuevo);
            cabeza = nuevo;
        } else {
            cabeza = new NodoDE(dato);
        }
    }
    public NodoDE irAlUltimo() {
        NodoDE resultado = cabeza;
        if (cabeza != null) {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                resultado = temp.getSiguiente();
                temp = temp.getSiguiente();
            }
            return resultado;
        } else {
            return resultado;
        }
    }

    public int contarNodos() {
        if (cabeza != null) {
            int cont = 0;
            NodoDE temp = cabeza;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();
            }
            return cont;
        }
        return 0;
    }

    public String listarNodos() {
        if (cabeza != null) {
            String listado = "";
            NodoDE temp = cabeza;
            while (temp != null) {
                listado += temp.getDato() + "\n";
                temp = temp.getSiguiente();
            }
            return listado;
        }
        return "No hay datos";
    }

     public boolean eliminarNodo(Vehiculo dato) {
        if (cabeza != null) {
            if (cabeza.getDato().getPlaca().equals(dato.getPlaca())) {
                cabeza = cabeza.getSiguiente();
                return true;
            } else {
                NodoDE temp = cabeza;
                while (temp != null) {
                    if (temp.getSiguiente() != null) {
                        if (temp.getSiguiente().getDato().getPlaca().equals(dato.getPlaca())) {
                            temp.setSiguiente(temp.getSiguiente().getSiguiente());
                            temp.getSiguiente().setAnterior(temp);
                            return true;
                        }
                    }
                    temp = temp.getSiguiente();
                }
            }
        }
        return false;
    }
}
