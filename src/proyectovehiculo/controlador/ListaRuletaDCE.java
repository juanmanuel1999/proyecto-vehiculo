/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectovehiculo.controlador;

import java.util.Random;
import proyectovehiculo.modelo.Ficha;
import proyectovehiculo.modelo.NodoCDE;

/**
 *
 * @author Usuario
 */
public class ListaRuletaDCE {

    private NodoCDE cabeza;

    public ListaRuletaDCE() {
    }

    public ListaRuletaDCE(NodoCDE cabeza) {
        this.cabeza = cabeza;
    }

    public NodoCDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoCDE cabeza) {
        this.cabeza = cabeza;
    }

    public String listarNodos() {
        if (cabeza != null) {
            NodoCDE temp = cabeza;
            String datos = "";
            do {
                datos = datos + temp.getDato().toString() + "\n";

                temp = temp.getSiguiente();
            } while (temp != cabeza);
            return datos;
        } else {
            return "no hay datos";
        }

    }

    // copia el del profesor lindo
    public int contarNodos() {
        if (cabeza != null) {
            NodoCDE temp = cabeza;
            int cont = 0;
            do {
                cont++;
                temp = temp.getSiguiente();
            } while (temp != cabeza);
            return cont;
        } else {
            return 0;
        }

    }
/*
    public int generarNumeroAleatorio() {
        Random aleatorio = new Random();
        int num = aleatorio.nextInt(contarNodos());
        return num;
    }
*/
    public String buscarNodoDerecha(int numeroNodo) {
        String dato = "";
        if(cabeza != null){
            NodoCDE temp = cabeza;
            int cont = 1;
            while(temp.getSiguiente() != null){
                if(numeroNodo == cont){
                    dato = temp.getDato().toString();
                    return dato;
                }
                cont++;
                temp = temp.getSiguiente();
            }
        }
        return "No hay ganador";
    }

    public void adicionaFichaFinal(Ficha dato) {
        if (cabeza != null) {
            NodoCDE ultimo = cabeza.getAnterior();
            NodoCDE nuevo = new NodoCDE(dato);
            nuevo.setSiguiente(cabeza);
            ultimo.setSiguiente(nuevo);
            nuevo.setAnterior(ultimo);
            cabeza.setAnterior(nuevo);
        } else {
            cabeza = new NodoCDE(dato);
            cabeza.setSiguiente(cabeza);
            cabeza.setAnterior(cabeza);

        }
    
    }

        

    public String buscarNodoIzquierda(int numeroNodo) {
              String dato = "";
        if(cabeza != null){
            NodoCDE temp = cabeza;
            int cont = 1;
            
            while(temp.getAnterior() != null){
                if(numeroNodo == cont){
                    dato = temp.getDato().toString();
                    return dato;
                }
                cont++;
                temp = temp.getAnterior();
            }
        }
        return "No hay ganador";
    }

}

