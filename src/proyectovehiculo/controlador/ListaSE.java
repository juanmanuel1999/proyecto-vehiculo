package proyectovehiculo.controlador;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyectovehiculo.modelo.Automovil;
import proyectovehiculo.modelo.Buseta;
import proyectovehiculo.modelo.Moto;
import proyectovehiculo.modelo.Nodo;
import proyectovehiculo.modelo.Vehiculo;
import javax.swing.JOptionPane;
import proyectovehiculo.controlador.excepciones.ParqueaderoExcepcion;

public class ListaSE {

    private Nodo cabeza;

    public ListaSE() {
    }

    public ListaSE(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    public int contarNodos() {
        if (cabeza != null) {
            int cont = 0;
            Nodo temp = cabeza;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();
            }
            return cont;
        }
        return 0;
    }

    public String listarNodos() {
        if (cabeza != null) {
            String listado = "";
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato() + "\n";
                temp = temp.getSiguiente();
            }
            return listado;
        }
        return "No hay datos";
    }

    public void adicionarNodoAlFinal(Vehiculo dato) throws ParqueaderoExcepcion 
    {
       
        if (cabeza != null) {
            verificarExistenciaVehiculo(dato);
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            temp.setSiguiente(new Nodo(dato));
        } else {
            cabeza = new Nodo(dato);
        }
    }

    public void adicionarNodoAlInicio(Vehiculo dato) {
        if (cabeza != null) {
            Nodo nuevo = new Nodo(dato);
            nuevo.setSiguiente(cabeza);
            cabeza = nuevo;
        } else {
            cabeza = new Nodo(dato);
        }
    }

    public void invertirLista() {
        Nodo temp = cabeza;
        ListaSE listaCopia = new ListaSE();
        while (temp != null) {
            listaCopia.adicionarNodoAlInicio(temp.getDato());
            temp = temp.getSiguiente();
        }
        cabeza = listaCopia.getCabeza();
    }

    public List<Vehiculo> listarNodosVehiculos() {
        if (cabeza != null) {
            List<Vehiculo> listado = new ArrayList<>();
            Nodo temp = cabeza;
            while (temp != null) {
                listado.add(temp.getDato());
                temp = temp.getSiguiente();
            }
            return listado;
        }
        return null;
    }

    public boolean eliminarNodo(Vehiculo dato) {
        if (cabeza != null) {
            if (cabeza.getDato().getPlaca().equals(dato.getPlaca())) {
                cabeza = cabeza.getSiguiente();
                return true;
            } else {
                Nodo temp = cabeza;
                while (temp != null) {
                    if (temp.getSiguiente() != null) {
                        if (temp.getSiguiente().getDato().getPlaca().equals(dato.getPlaca())) {
                            temp.setSiguiente(temp.getSiguiente().getSiguiente());
                            return true;
                        }
                    }
                    temp = temp.getSiguiente();
                }
            }
        }
        return false;
    }

    public Nodo irAlUltimo() {
        Nodo resultado = cabeza;
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                resultado = temp.getSiguiente();
                temp = temp.getSiguiente();
            }
            return resultado;
        } else {
            return resultado;
        }
    }

    //---------------------------------------- Nodos Que Faltan-----------------------------------------
    public boolean eliminarNodoxPosicion(int posicion) {
        int cont = 1;
        if (cabeza != null) {
            if (posicion == 1) {
                cabeza = cabeza.getSiguiente();
                return true;
            } else {
                Nodo temp = cabeza;
                while (temp.getSiguiente() != null) {
                    cont++;
                    if (posicion == cont) {
                        temp.setSiguiente(temp.getSiguiente().getSiguiente());
                        return true;
                    }
                    temp = temp.getSiguiente();
                }
            }
        }
        return false;
    }

    public void imprimirListaDePlacaConSuPosicion() {//Metodo inventado para sacar solo placas y su posicion
        String datos = "";
        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 0;
            while (temp.getSiguiente() != null) {
                cont++;
                datos += temp.getSiguiente().getDato().getPlaca() + "\n";
                temp = temp.getSiguiente();
            }
            JOptionPane.showMessageDialog(null, datos + "Su posicion es: ");
        } else {
            JOptionPane.showMessageDialog(null, "No se tienen datos");
        }
    }

    public int buscarCiudadPorCiudad(String ciudad) {
        int cont = 0;
        if (cabeza != null) {
            Nodo temp = cabeza;
            if (temp.getDato() != null) {
                if (temp.getDato().getCiudad().equals(ciudad)) {
                    cont++;
                }
            }
            while (temp != null) {
                if (temp.getSiguiente() != null) {
                    if (temp.getSiguiente().getDato().getCiudad().equals(ciudad)) {
                        cont++;
                    }
                }
                temp = temp.getSiguiente();
            }
        }
        return cont;
    }

    public boolean esImpar(int Numero) {
        if (Numero % 2 != 0) {
            return true;
        } else {
            return false;
        }
    }

    public void invertirExtremos() {
        int total = contarNodos();
        Nodo temp = cabeza;
        int cont = 1;
        if (cabeza != null) {
            if (esImpar(total) == true) {
                if (total > 1) {
                    while (cont < (total - 1)) {
                        temp = temp.getSiguiente();
                        cont++;
                    }
                    Nodo temp2 = temp.getSiguiente();
                    temp.setSiguiente(cabeza);
                    temp2.setSiguiente(cabeza.getSiguiente());
                    cabeza.setSiguiente(null);
                    cabeza = temp2;
                }
            } else {
                contarNodos();
            }
        }
    }

    public void invertirNodosInternos() {
        int nodos = contarNodos();
        if (esImpar(nodos) == false) {
            if (nodos > 2) {
                Nodo temp1 = cabeza;
                Nodo temp2 = cabeza;
                Nodo temp3 = cabeza;
                nodos = (nodos / 2) - 1;
                int cont = 0;
                while (nodos == cont) {
                    cont++;
                    temp1 = temp1.getSiguiente();
                }
                temp2 = temp1.getSiguiente();
                temp3 = temp2.getSiguiente();
                temp2.setSiguiente(temp3.getSiguiente());
                temp1.setSiguiente(temp3);
                temp3.setSiguiente(temp2);
            }
        } else {
            contarNodos();
        }
    }

    public void metodoQueEliminaPosicionesImpares() {
        ListaSE listaCopia = new ListaSE();
        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 0;
            while (temp != null) {
                cont++;
                if (esImpar(cont) != true) {
                    try {
                        listaCopia.adicionarNodoAlFinal(temp.getDato());
                    } catch (ParqueaderoExcepcion ex) {
                        Logger.getLogger(ListaSE.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    contarNodos();
                }
                temp = temp.getSiguiente();
            }
        }
        cabeza = listaCopia.getCabeza();
    }

    public int numerosDeMoto() {
        Nodo temp = cabeza;
        int contMoto = 0;
        if (cabeza != null) {
            if (temp.getDato() instanceof Moto) {
                contMoto++;
            }
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
                if (temp.getDato() instanceof Moto) {
                    contMoto++;
                }
            }
        }
        return contMoto;
    }

    public int numerosDeAutomovil() {
        Nodo temp = cabeza;
        int contAutomovil = 0;
        if (cabeza != null) {
            if (temp.getDato() instanceof Automovil) {
                contAutomovil++;
            }
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
                if (temp.getDato() instanceof Automovil) {
                    contAutomovil++;
                }

            }
        }
        return contAutomovil;
    }

    public int numerosDeBuses() {
        Nodo temp = cabeza;
        int contBuses = 0;
        if (cabeza != null) {
            if (temp.getDato() instanceof Buseta) {
                contBuses++;
            }
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
                if (temp.getDato() instanceof Buseta) {
                    contBuses++;
                }
            }
        }
        return contBuses;
    }

    public void verificarExistenciaVehiculo(Vehiculo veh) throws ParqueaderoExcepcion {
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp != null) {

                if (temp.getDato().getPlaca().equals(veh.getPlaca())) {
                    throw new ParqueaderoExcepcion("El vehiculo ya existe");
                }
                temp = temp.getSiguiente();
            }
        }

    }

}
